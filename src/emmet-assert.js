import {parser, loadSnippets} from 'emmet';

const MODE_SEARCH = Symbol('search mode');
const MODE_ASSERTION = Symbol('assertion mode');
const FAIL = Symbol('failure flag');
const {parse} = parser;
const identity = item => item;

function elementHasTextContent(element, text, currentPathString, mode) {
    const isValid = !text || element.props.children === text;

    if (!isValid) {
        return handleFailureForMode(`${currentPathString} expected text content ${text}, found ${element.props.children}`, mode);
    }
}

function getPathStringFromList(pathList) {
    const position = pathList.map(index => `[${index}]`).join(',');

    return `(at ${position})`;
}

function handleFailureForMode(caption, mode) {
    if (mode === MODE_ASSERTION) {
        throw new Error(caption);
    }
    return FAIL;
}

function testNode(node, element, currentPathList, mode) {
    const pathListString = getPathStringFromList(currentPathList);

    if (!element) {
        const missingNodeFailure = handleFailureForMode(`${pathListString} expected node ${node.name()}, no node found`, mode);

        if (missingNodeFailure) {
            return missingNodeFailure;
        }
    } else if (node.name() === element.type) {
        const attributes = node.attributeList();
        const attributesMatch = attributes.map(attribute => {
            const attributeName = (attribute.name === 'class' ? 'className' : attribute.name);

            if(element.props[attributeName] !== attribute.value) {
                return handleFailureForMode(`${pathListString} expected attribute ${attributeName} ${attribute.value}, found ${element.props[attributeName]}`, mode);
            }
        }).find(identity);

        if (attributesMatch) {
            return attributesMatch;
        }

        const textMatchFailure = elementHasTextContent(element, node.content, pathListString, mode);

        if (textMatchFailure) {
            return textMatchFailure;
        }
    } else {
        return handleFailureForMode(`${pathListString} expected node ${node.name()}, found ${element.type}`, mode);
    }

    return element;
}

const last = (items) => {
    if (items.length > 0) {
        return items.slice(-1)[0];
    }
}

function testNodeChildren(nodes, elements, pathIndexList, mode) {
    const matches = nodes.map((node, index) => {
        const currentPathList = [...pathIndexList, index];
        const element = elements[index];

        const results = testNode(node, element, currentPathList, mode);

        if (results === FAIL) {
            return FAIL;
        } else if (mode === MODE_SEARCH && index === nodes.length - 1 && !node.children.length && results) {
            return element;
        }

        const {children: propsChildren} = element.props;
        const elementChildren = propsChildren instanceof Array ? propsChildren : [propsChildren];

        return testNodeChildren(node.children, elementChildren, currentPathList, mode);
    });
    const hasFailure = matches.find(x => x === FAIL);

    if (hasFailure) {
        return hasFailure;
    }

    return last(matches);
}

export default function emmetAssert(element) {
    loadSnippets({
        html: {
            abbreviations: {
                a: '<a>',
                img: '<img />',
                form: '<form>',
                input: '<input />',
                label: '<label>'
            }
        }
    });

    return {
        includes: function (emmetPath) {
            const structure = parse(emmetPath);

            return testNodeChildren(structure.children, [element], [], MODE_ASSERTION);
        },
        find: function (emmetPath) {
            const structure = parse(emmetPath);
            const results = testNodeChildren(structure.children, [element], [], MODE_SEARCH);

            if (results === FAIL) {
                return undefined;
            }
            return results;
        }
    }
}
