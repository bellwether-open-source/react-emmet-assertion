# react-emmet-assertion

## Description
This library exists to make the testing of structural output from React simple.  Most approaches to structural tests tend to require the use of shallow-rendering frameworks and indirect behavioral testing through simulated events.  This library attempts to offer a less involved process for testing output from React components.

## Before you begin
Due to the way Emmet works out-of-the-box, some abbreviations implicitly provide placeholder attributes (e.g. `a` resolves to `<a href="">`).  As this behavior makes it difficult to test-drive DOM creation without having all of the relevant details up-front, the react-emmet-assertion library makes some adjustments to Emmet's defaults:

* `a` is rewritten from `<a href="">` to `<a>`
* `img` is rewritten from `<img src="" alt="" />` to `<img />`
* `form` is rewritten from `<form action="">` to `<form>`
* `input` is rewritten from `<input type="text" />` to `<input />`

As the above changes affect the Emmet library as a whole, consuming this assertion library **in a project that already utilizes Emmet** may have unintended side-effects.  This fact is documented as [an issue](https://gitlab.com/bellwether-open-source/react-emmet-assertion/issues/2).

## Usage
This library directly tests the JSX component tree from a component's `.render()` method (or the direct output from pure functional components).  The `emmetAssert` function wraps this output and permits emmet expressions to be evaluated against it:

```js
import emmetAssert from 'react-emmet-assertion';

const FunctionalThing = (props) => <div className={props.className}>{props.caption}</div>;
const renderedComponent = FunctionalThing({className: 'test', caption: 'Hello'});

const wrapped = emmetAssert(renderedComponent);
```

From there, it is possible to perform assertions or to query for a specific node:

### Tests (`.includes`)
To assert that the wrapped component matches the provided Emmet path:

```js
const emmetPath = 'div.test{Hello}';

wrapped.includes(emmetPath); // should not throw an error, perfect match
wrapped.includes('div'); // also should not throw an error, inclusive match
wrapped.includes('span'); // throws "Error: (at [0]) expected node span, found div"
wrapped.includes('div>span'); // throws "Error: (at [0],[0]) expected node span, found undefined"
```

Error output from `.includes` failures attempt to specify the location of the failure, to better facilitate diagnosis of failures or for TDD that is driven by error output.  The format of this message is given as:

> `Error: (at <path>) <reason>`

...where `<path>` is a comma-delimited series of zero-indexed identifiers that describe the node under test, and `<reason>` is an explanation of which test failed.

### Queries (`.find`)
The wrapped object also exposes a `.find` method that, like `.includes`, accepts an emmet path.  Instead of performing an assertion, though, the utility will return the last matching React element.  The intent of this is that developers may wish for opportunity to perform tests against nodes that are not satisfactorily provided by the emmet assertions (e.g. testing the qualities of props passed to child components).

```js
const element = <section className="main">
    <ul>
        <li>
            <label>
                <span>1</span>
                <input type="checkbox" name="items[]" value="1" />
            </label>
        </li>
        <li>
            <label>
                <span>2</span>
                <input type="checkbox" name="items[]" value="2" />
            </label>
        </li>
        <li>
            <label>
                <span></span>
                <input type="checkbox" name="items[]" value="1" />
            </label>
        </li>
        <li>
            <label>
                <span></span>
                <input type="checkbox" name="items[]" value="1" />
            </label>
        </li>
        <li>
            <label>
                <span></span>
                <input type="checkbox" name="items[]" value="1" />
            </label>
        </li>
    </ul>
</section>;

const wrapped = emmetAssert(element);
const spanWithTwo = wrapped.find('section>ul>li*2>label>span');  // should return <span>2</span>
```
