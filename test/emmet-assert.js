import React from 'react';
import {expect} from 'chai';
import emmetAssert from '../src/emmet-assert';

function setupInputsOutputs(ctx) {
    return () => {
        ctx.inputs = {};
        ctx.outputs = {};
    }
}

function runAssertion(ctx) {
    return () => {
        const {element, emmetString} = ctx.inputs;

        ctx.outputs.results = emmetAssert(element).includes(emmetString);
    };
}

function runQuery(ctx) {
    return () => {
        const {element, emmetString} = ctx.inputs;

        ctx.outputs.results = emmetAssert(element).find(emmetString);
    };
}

function getReactElementForOverrides(ctx) {
    return () => {
        ctx.inputs.element = (
            <a>
                <img />
                <form />
                <input />
            </a>
        );
    }
}

function getSimpleReactElement(ctx) {
    return () => {
        ctx.inputs.element = (
            <div className="bob">
                <span rel="sup">Hello</span>
            </div>
        );
    };
}

function getComplexReactElement(ctx) {
    return () => {
        ctx.inputs.element = (
            <div className="bob">
                <div id="richard"></div>
                <div id="harry"></div>
                <span rel="sup">Hello</span>
            </div>
        );
    };
}

function getListReactElement(ctx) {
    return () => {
        ctx.inputs.element = (
            <div>
                <ul>
                    <li id="li-1">
                        <span rel="skip-me">1</span>
                        <span>2</span>
                    </li>
                    <li id="li-2">
                        <span rel="target">1</span>
                        <span>2</span>
                    </li>
                    <li id="li-3">
                        <span rel="never-reach-me">1</span>
                        <span>2</span>
                    </li>
                </ul>
            </div>
        );
    };
}

describe('Feature: React Emmet Assertion', () => {
    describe('Background: Assertion library provides Emmet overrides', () => {
        let ctx = {};

        beforeEach('Setup: inputs/outputs', setupInputsOutputs(ctx));

        beforeEach('Given React element', getReactElementForOverrides(ctx));
        beforeEach('Given emmet test string that matches given element', () => {
            ctx.inputs.emmetString = 'a>img+form+input';
        });

        it('Then should successfully match', () => {
            expect(runAssertion(ctx)).not.to.throw(Error);
        });
    });

    describe('Scenario: inclusive structure assertion with match', () => {
        let ctx = {};

        beforeEach('Setup: inputs/outputs', setupInputsOutputs(ctx));

        beforeEach('Given simple React element', getSimpleReactElement(ctx));
        beforeEach('Given emmet test string that matches given element', () => {
            ctx.inputs.emmetString = 'div.bob>span[rel=sup]{Hello}';
        });

        it('Then should indicate whether the element structure has a match', () => {
            expect(runAssertion(ctx)).not.to.throw(Error);
        });
    });

    describe('Scenario: failing match for nodes', () => {
        let ctx = {};

        beforeEach('Setup: inputs/outputs', setupInputsOutputs(ctx));

        beforeEach('Given simple React element', getSimpleReactElement(ctx));
        beforeEach('Given emmet test string that doesn\'t match given element', () => {
            ctx.inputs.emmetString = 'div>div';
        });

        it('Then should indicate whether the element structure has a match', () => {

            expect(runAssertion(ctx)).to.throw('(at [0],[0]) expected node div, found span');
        });
    });

    describe('Scenario: failing match for className attribute', () => {
        let ctx = {};

        beforeEach('Setup: inputs/outputs', setupInputsOutputs(ctx));

        beforeEach('Given simple React element', getSimpleReactElement(ctx));
        beforeEach('Given emmet test string that doesn\'t match given element', () => {
            ctx.inputs.emmetString = 'div>span.bob[rel="sup"]';
        });


        it('Then should indicate whether the element structure has a match', () => {
            expect(runAssertion(ctx)).to.throw('(at [0],[0]) expected attribute className bob, found undefined');
        });
    });

    describe('Scenario: failing match for other attribute', () => {
        let ctx = {};

        beforeEach('Setup: inputs/outputs', setupInputsOutputs(ctx));

        beforeEach('Given simple React element', getSimpleReactElement(ctx));
        beforeEach('Given emmet test string that doesn\'t match given element', () => {
            ctx.inputs.emmetString = 'div>span[rel="sud"]';
        });


        it('Then should indicate whether the element structure has a match', () => {
            expect(runAssertion(ctx)).to.throw('(at [0],[0]) expected attribute rel sud, found sup');
        });
    });

    describe('Scenario: failing match for text content', () => {
        let ctx = {};

        beforeEach('Setup: inputs/outputs', setupInputsOutputs(ctx));

        beforeEach('Given simple React element', getSimpleReactElement(ctx));
        beforeEach('Given emmet test string that doesn\'t match given element', () => {
            ctx.inputs.emmetString = 'div>span{Hallo}';
        });


        it('Then should indicate whether the element structure has a match', () => {
            expect(runAssertion(ctx)).to.throw('(at [0],[0]) expected text content Hallo, found Hello');
        });
    });

    describe('Scenario: passing match for siblings', () => {
        let ctx = {};

        beforeEach('Setup: inputs/outputs', setupInputsOutputs(ctx));

        beforeEach('Given simple React element', getComplexReactElement(ctx));
        beforeEach('Given emmet test string that doesn\'t match given element', () => {
            ctx.inputs.emmetString = 'div>div*2';
        });

        it('Then should indicate whether the element structure has a match', () => {
            expect(runAssertion(ctx)).not.to.throw(Error);
        });
    });

    describe('Scenario: Missing element in actual that was declared in assertion', () => {
        let ctx = {};

        beforeEach('Setup: inputs/outputs', setupInputsOutputs(ctx));

        beforeEach('Given simple React element', getComplexReactElement(ctx));
        beforeEach('Given emmet test string that doesn\'t match given element', () => {
            ctx.inputs.emmetString = 'div>div+div+span+h3';
        });

        it('Then should indicate whether the element structure has a match', () => {
            expect(runAssertion(ctx)).to.throw('(at [0],[3]) expected node h3, no node found');
        });
    });

    describe('Scenario: Actual and expected match for complex element', () => {
        let ctx = {};

        beforeEach('Setup: inputs/outputs', setupInputsOutputs(ctx));

        beforeEach('Given simple React element', getComplexReactElement(ctx));
        beforeEach('Given emmet test string that matches given element', () => {
            ctx.inputs.emmetString = 'div>div+div+span';
        });

        it('Then should indicate whether the element structure has a match', () => {
            expect(runAssertion(ctx)).not.to.throw();
        });
    });
});

describe('Feature: React Emmet Query', () => {
    describe('Scenario: searching for element that exists', () => {
        let ctx = {};

        beforeEach('Setup: inputs/outputs', setupInputsOutputs(ctx));

        beforeEach('Given complex React element', getComplexReactElement(ctx));
        beforeEach('Given emmet test string that matches given element', () => {
            ctx.inputs.emmetString = 'div>div+div+span';
        });

        beforeEach('When a query is performed for the specified element', runQuery(ctx));

        it('Then should locate and return <span rel="sup">Hello</span>', () => {
            const span = ctx.outputs.results;

            expect(span.type).to.equal('span');
            expect(span.props.rel).to.equal('sup');
            expect(span.props.children).to.equal('Hello');
        });
    });

    describe('Scenario: searching for repeating element that exists', () => {
        let ctx = {};

        beforeEach('Setup: inputs/outputs', setupInputsOutputs(ctx));

        beforeEach('Given complex React element', getListReactElement(ctx));
        beforeEach('Given emmet test string that matches given element', () => {
            ctx.inputs.emmetString = 'div>ul>li*2>span+span';
        });

        beforeEach('When a query is performed for the specified element', runQuery(ctx));

        it('Then should locate and return <span>{2}</span>', () => {
            const span = ctx.outputs.results;

            expect(span.type).to.equal('span');
            expect(span.props.children).to.equal('2');
        });
    });

    describe('Scenario: searching for element that does not match', () => {
        let ctx = {};

        beforeEach('Setup: inputs/outputs', setupInputsOutputs(ctx));

        beforeEach('Given complex React element', getComplexReactElement(ctx));
        beforeEach('Given emmet test string that matches given element', () => {
            ctx.inputs.emmetString = 'div>div+div+h3';
        });

        beforeEach('When a query is performed for the specified element', runQuery(ctx));

        it('Then should return undefined', () => {
            expect(ctx.outputs.results).to.equal(undefined);
        });
    });

    describe('Scenario: searching for attribute that does not match', () => {
        let ctx = {};

        beforeEach('Setup: inputs/outputs', setupInputsOutputs(ctx));

        beforeEach('Given complex React element', getComplexReactElement(ctx));
        beforeEach('Given emmet test string that matches given element', () => {
            ctx.inputs.emmetString = 'div>div+div+span.bob';
        });

        beforeEach('When a query is performed for the specified element', runQuery(ctx));

        it('Then should return undefined', () => {
            expect(ctx.outputs.results).to.equal(undefined);
        });
    });

    describe('Scenario: searching for text node that does not match', () => {
        let ctx = {};

        beforeEach('Setup: inputs/outputs', setupInputsOutputs(ctx));

        beforeEach('Given complex React element', getComplexReactElement(ctx));
        beforeEach('Given emmet test string that matches given element', () => {
            ctx.inputs.emmetString = 'div>div+div+span{Hi}';
        });

        beforeEach('When a query is performed for the specified element', runQuery(ctx));

        it('Then should return undefined', () => {
            expect(ctx.outputs.results).to.equal(undefined);
        });
    });

    describe('Scenario: searching for text node that does not exist', () => {
        let ctx = {};

        beforeEach('Setup: inputs/outputs', setupInputsOutputs(ctx));

        beforeEach('Given complex React element', getComplexReactElement(ctx));
        beforeEach('Given emmet test string that matches given element', () => {
            ctx.inputs.emmetString = 'div>div+div+span+h3';
        });

        beforeEach('When a query is performed for the specified element', runQuery(ctx));

        it('Then should return undefined', () => {
            expect(ctx.outputs.results).to.equal(undefined);
        });
    });
});
