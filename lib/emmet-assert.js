'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = emmetAssert;

var _emmet = require('emmet');

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var MODE_SEARCH = Symbol('search mode');
var MODE_ASSERTION = Symbol('assertion mode');
var FAIL = Symbol('failure flag');
var parse = _emmet.parser.parse;

var identity = function identity(item) {
    return item;
};

function elementHasTextContent(element, text, currentPathString, mode) {
    var isValid = !text || element.props.children === text;

    if (!isValid) {
        return handleFailureForMode(currentPathString + ' expected text content ' + text + ', found ' + element.props.children, mode);
    }
}

function getPathStringFromList(pathList) {
    var position = pathList.map(function (index) {
        return '[' + index + ']';
    }).join(',');

    return '(at ' + position + ')';
}

function handleFailureForMode(caption, mode) {
    if (mode === MODE_ASSERTION) {
        throw new Error(caption);
    }
    return FAIL;
}

function testNode(node, element, currentPathList, mode) {
    var pathListString = getPathStringFromList(currentPathList);

    if (!element) {
        var missingNodeFailure = handleFailureForMode(pathListString + ' expected node ' + node.name() + ', no node found', mode);

        if (missingNodeFailure) {
            return missingNodeFailure;
        }
    } else if (node.name() === element.type) {
        var attributes = node.attributeList();
        var attributesMatch = attributes.map(function (attribute) {
            var attributeName = attribute.name === 'class' ? 'className' : attribute.name;

            if (element.props[attributeName] !== attribute.value) {
                return handleFailureForMode(pathListString + ' expected attribute ' + attributeName + ' ' + attribute.value + ', found ' + element.props[attributeName], mode);
            }
        }).find(identity);

        if (attributesMatch) {
            return attributesMatch;
        }

        var textMatchFailure = elementHasTextContent(element, node.content, pathListString, mode);

        if (textMatchFailure) {
            return textMatchFailure;
        }
    } else {
        return handleFailureForMode(pathListString + ' expected node ' + node.name() + ', found ' + element.type, mode);
    }

    return element;
}

var last = function last(items) {
    if (items.length > 0) {
        return items.slice(-1)[0];
    }
};

function testNodeChildren(nodes, elements, pathIndexList, mode) {
    var matches = nodes.map(function (node, index) {
        var currentPathList = [].concat(_toConsumableArray(pathIndexList), [index]);
        var element = elements[index];

        var results = testNode(node, element, currentPathList, mode);

        if (results === FAIL) {
            return FAIL;
        } else if (mode === MODE_SEARCH && index === nodes.length - 1 && !node.children.length && results) {
            return element;
        }

        var propsChildren = element.props.children;

        var elementChildren = propsChildren instanceof Array ? propsChildren : [propsChildren];

        return testNodeChildren(node.children, elementChildren, currentPathList, mode);
    });
    var hasFailure = matches.find(function (x) {
        return x === FAIL;
    });

    if (hasFailure) {
        return hasFailure;
    }

    return last(matches);
}

function emmetAssert(element) {
    (0, _emmet.loadSnippets)({
        html: {
            abbreviations: {
                a: '<a>',
                img: '<img />',
                form: '<form>',
                input: '<input />',
                label: '<label>'
            }
        }
    });

    return {
        includes: function includes(emmetPath) {
            var structure = parse(emmetPath);

            return testNodeChildren(structure.children, [element], [], MODE_ASSERTION);
        },
        find: function find(emmetPath) {
            var structure = parse(emmetPath);
            var results = testNodeChildren(structure.children, [element], [], MODE_SEARCH);

            if (results === FAIL) {
                return undefined;
            }
            return results;
        }
    };
}